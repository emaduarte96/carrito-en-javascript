// Variables 
const carrito = document.querySelector('#carrito');
const contenedorCarrito = document.querySelector('#lista-carrito tbody');
const vaciarCarritoBtn = document.querySelector('#vaciar-carrito');
const listaCursos = document.querySelector('#lista-cursos');
let articulosCarrito = [];

cargarEventos();

function cargarEventos (){
        //Agregar Elementos al carrito
        listaCursos.addEventListener('click',agregarCursos);

        //Elimina cursos del carrito
        carrito.addEventListener('click', eliminarCurso);

        //Vaciar carrito
        vaciarCarritoBtn.addEventListener('click', () =>{
            articulosCarrito = []; // Reseteamos el arreglo

            limpiarHTML(); // Eliminamos todo el HTML
        })

}

//Funciones
function agregarCursos(e){  
    e.preventDefault(); 
    if(e.target.classList.contains('agregar-carrito')){
        const curso5eleccionado = e.target.parentElement.parentElement;
        leerDatosCursos(curso5eleccionado);
        }
    }


//Elimina un curso del carrito
function eliminarCurso(e){
    if(e.target.classList.contains('borrar-curso')){
        const cursoId = e.target.getAttribute('data-id');
        articulosCarrito = articulosCarrito.filter(curso => curso.id !== cursoId);
        carritoHTML();
    }

}

//Lee el contenido al que le dimos click
function leerDatosCursos(curso){
    console.log(curso);
    
    //Creamos objeto
    const infoCurso = {
        imagen: curso.querySelector('img').src,
        titulo: curso.querySelector('h4').textContent,
        precio: curso.querySelector('.precio span').textContent,
        id: curso.querySelector('a').getAttribute('data-id'),
        cantidad: 1,
    }
    
    //Revisa si un elemento ya existe en el carrito
    const existe = articulosCarrito.some(curso => curso.id === infoCurso.id);
    if(existe){
        const cursos = articulosCarrito.map(curso =>{
            if(curso.id === infoCurso.id){
                curso.cantidad++;
                return curso;
            } else {
                return curso;
            }
        });
        articulosCarrito = [...cursos];

    } else {
        //Agrega elementos al arreglo
        articulosCarrito = [...articulosCarrito, infoCurso];
    }


    console.log(articulosCarrito);
    carritoHTML()
    }

//Muestra el carrito de compras en el HTML
function carritoHTML(){
    //limpia el HTML
    limpiarHTML();

    //Agregar HTML en el Tbody
    articulosCarrito.forEach( curso =>{
        const {imagen, titulo, precio, cantidad, id} = curso;
        const row = document.createElement('tr');
            row.innerHTML=`
            <td><img src='${imagen}' width=130px></td>
            <td>${titulo}</td>
            <td>${precio}</td>
            <td>${cantidad}</td>
            <td><a href='#' class='borrar-curso' data-id='${id}'> X </a></td>
            `
        //Agrego el HTML al carrito
        contenedorCarrito.appendChild(row);
    })
}

//Elimino los cursos del tbody
function limpiarHTML(){
   while(contenedorCarrito.firstChild){
    contenedorCarrito.removeChild(contenedorCarrito.firstChild);
   }
}